/* Copyright 2020, Pipe17, Inc.
 *
 * Logging functions
 */

const log_levels = {
    "debug": true,
    "info": true,
    "warn": true,
    "error": true
};

const log_debug = function (s) {
    if (log_levels.debug) console.debug(s);
};
const log_info = function (s) {
    if (log_levels.info) console.info(s);
};
const log_warn = function (s) {
    if (log_levels.warn) console.log(s);
};
const log_error = function (s) {
    if (log_levels.error) console.error("Error: " + s);
};

module.exports = {
    log_levels,
    log_debug,
    log_info,
    log_warn,
    log_error
};
