const mongodb = require('mongodb').MongoClient
const {log_debug, log_error} = require('./logging');
const {config} = require('./config')


const execMongo = async function (mongodb_cmd) {
    try {
        const client = await getMongoClient();
        if (client) {
            const db = client.db(config.mongo_db_name);
            const res = await eval(mongodb_cmd);
            return res
        }
    } catch (err) {
        return {"error": err.message}
    }
}

/* Retrieves the key used to access the database directly.
 * The key is cached in a file and in a local variable.  The file cache may
 * be used by other lambda invocations if they execute in the same container.
 */
let mongoClient; // Do not initialize, reusing this (warm) node retains value
const getMongoClient = async function () {
    log_debug("In getMongoClient: fetch connection from variable");
    if (mongoClient)
        return mongoClient;
    try {
        log_debug("In getMongoClient: create new connection");
        const mongo_url = config.mongo_uri
        const mongo_options = {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            readPreference: 'secondaryPreferred',
            writeConcern: {w: 3, j: true, wtimeout: config.mongo_connection_exec_timeout_ms},
            retryWrites: true,
        };
        log_debug("In getMongoClient: calling mongodb.connect");
        mongoClient = await mongodb.connect(mongo_url, mongo_options);
        log_debug("In getMongoClient: mongodb.connect returned");
        return mongoClient;
    } catch (err) {
        log_error("In getMongoClient: " + err.message);
        return null;
    }
};

module.exports = {
    execMongo,
    getMongoClient
}